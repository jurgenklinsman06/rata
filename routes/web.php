<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/test-satu', 'TestSatuController@index');

$router->get('/customers', 'CustomerController@index');
$router->post('/customer', 'CustomerController@store');
$router->get('/customer/{id}', 'CustomerController@customer');
$router->post('/customer/{id}', 'CustomerController@edit');
$router->delete('/customer/{id}', 'CustomerController@delete');

$router->get('/goods', 'GoodsController@index');
$router->post('/goods', 'GoodsController@store');
$router->get('/goods/{id}', 'GoodsController@goods');
$router->post('/goods/{id}', 'GoodsController@edit');
$router->delete('/goods/{id}', 'GoodsController@delete');

$router->post('/customer/goods/booking', 'TransactionController@store');
$router->post('/customer/goods/pay', 'TransactionController@pay');

$router->get('/production/goods', 'TransactionController@productions');
$router->get('/production/goods/{id}', 'TransactionController@production');

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DatabaseDesign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('address');
            $table->timestamps();
        });

        Schema::create('goods', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->string('name');
            $table->integer('price');
            $table->integer('total');
            $table->timestamps();
        });

        Schema::create('transactions', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->string('customer_id', 20);
            $table->string('payment_id')->nullable();
            $table->string('status', 20);
            $table->timestamp('payment_date')->nullable();
            $table->string('payment_proof')->nullable();
            $table->integer('total_price')->nullable();
            $table->timestamps();
        });

        Schema::create('transaction_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id', 20);
            $table->string('goods_id');
            $table->string('total_goods');
            $table->string('price');
            $table->string('total_paid')->nullable();
            $table->timestamps();
        });

        Schema::create('productions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('transaction_detail_id');
            $table->string('status', 20);
            $table->text('notes');
            $table->date('date_in');
            $table->date('production_date');
            $table->date('finish_date');
            $table->timestamps();
        });

        Schema::create('payment_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
        Schema::dropIfExists('goods');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('transaction_detail');
        Schema::dropIfExists('productions');
    }
}

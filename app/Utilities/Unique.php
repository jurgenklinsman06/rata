<?php

namespace App\Utilities;

use Illuminate\Support\Facades\DB;

class Unique
{
    public static function uniqueID($table, $col, $initial, $length = 8)
    {
        do {
            $alpha = str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ');
            $numeric = str_shuffle('23456789');
            $generatedCode = $initial . '-' . substr(str_shuffle($alpha), 0, 2) . substr(str_shuffle($numeric), 0, 2) . substr(str_shuffle($alpha), 0, 2);
            $baseTable = DB::table($table)->where($col, $generatedCode)->first();
        } while (null !== $baseTable);

        return $generatedCode;
    }
}

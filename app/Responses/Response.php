<?php

namespace App\Responses;

use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use App\Formatters\ErrorMessageFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;

class Response
{
    public static function send(int $code, $data = null, string $message = null, array $headers = [])
    {
        $response = [];

        if (null !== $data) {
            $response['data'] = $data;
        }

        if ($data instanceof MessageBag) {
            $response['data'] = ErrorMessageFormatter::format($data);
        }

        if (null !== $message) {
            $response['message'] = $message;
        }

        $response['timezone'] = env('APP_TIMEZONE');
        $response['datetime'] = Carbon::now()->format(env('APP_DATETIME_FORMAT'));
        $response['epochtime'] = Carbon::now()->timestamp;

        if ('testing' !== env('APP_ENV')) {
            $response['ExecutionTime'] = round(microtime(true) * 1000) - START . ' ms';
        }

        $result = (new JsonResponse($response, $code, $headers))->setEncodingOptions(JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $result;
    }

    public static function message(string $message)
    {
        return static::send(400, null, $message);
    }
}

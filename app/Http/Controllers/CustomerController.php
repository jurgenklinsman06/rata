<?php

namespace App\Http\Controllers;

use App\Responses\Response;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::all();

        return Response::send(200, $customers);
    }

    public function customer(int $id)
    {
        $customer = Customer::find($id);

        if (null == $customer) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        return Response::send(200, $customer);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'address' => 'required|string'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::send(422, $validator->errors());
        }

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->save();

        return Response::send(200, $customer);
    }

    public function edit(Request $request, int $id)
    {
        $customer = Customer::find($id);

        if (null == $customer) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->save();

        return Response::send(200, $customer);
    }

    public function delete(int $id)
    {
        $customer = Customer::find($id);

        if (null == $customer) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        $customer->forceDelete();

        return Response::send(200);
    }
}

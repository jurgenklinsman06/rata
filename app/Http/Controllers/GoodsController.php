<?php

namespace App\Http\Controllers;

use App\Responses\Response;
use App\Models\Goods;
use App\Utilities\Unique;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GoodsController extends Controller
{
    public function index(Request $request)
    {
        $goods = Goods::all();

        return Response::send(200, $goods);
    }

    public function goods(string $id)
    {
        $goods = Goods::find($id);

        if (null == $goods) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        return Response::send(200, $goods);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'price' => 'required|integer',
            'total' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::send(422, $validator->errors());
        }

        $goods = new Goods();
        $goods->id = Unique::uniqueID('goods', 'id', 'GTX');
        $goods->name = $request->name;
        $goods->price = $request->price;
        $goods->total = $request->total;
        $goods->save();

        return Response::send(200, $goods);
    }

    public function edit(Request $request, string $id)
    {
        $goods = Goods::find($id);

        if (null == $goods) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        $goods->name = $request->name;
        $goods->price = $request->price;
        $goods->total = $request->total;
        $goods->save();

        return Response::send(200, $goods);
    }

    public function delete(string $id)
    {
        $goods = Goods::find($id);

        if (null == $goods) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        $goods->forceDelete();

        return Response::send(200);
    }
}

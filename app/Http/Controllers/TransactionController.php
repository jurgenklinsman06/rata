<?php

namespace App\Http\Controllers;

use App\Responses\Response;
use App\Models\Transaction;
use App\Models\Production;
use App\Models\TransactionDetail;
use App\Utilities\Unique;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class TransactionController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'customer_id' => 'required|integer',
            'goods' => 'required|array',
            'goods.*.id' => 'required|string',
            'goods.*.total' => 'required|integer',
            'goods.*.price' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::send(422, $validator->errors());
        }

        $totalPrice = 0;

        foreach ($request->goods as $goods) {
            $totalPrice = $totalPrice + ($goods['total'] * $goods['price']);
        }

        $transaction = new Transaction();
        $transaction->id = Unique::uniqueID('transactions', 'id', 'GTX');
        $transaction->customer_id = $request->customer_id;
        $transaction->status = 'pending';
        $transaction->total_price = $totalPrice;
        $transaction->save();

        foreach ($request->goods as $goods) {
            $transactionDetail = new TransactionDetail();
            $transactionDetail->transaction_id = $transaction->id;
            $transactionDetail->goods_id = $goods['id'];
            $transactionDetail->total_goods = $goods['total'];
            $transactionDetail->price = $goods['price'];
            $transactionDetail->save();
        }

        return Response::send(200);
    }

    public function pay(Request $request)
    {
        $rules = [
            'transaction_id' => 'required|string',
            'payment_id' => 'required|integer',
            'payment_proof' => 'required|mimes:jpeg,png,jpg,gif,svg,heic|max:10240',
            'total_price' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::send(422, $validator->errors());
        }

        $transaction = Transaction::find($request->transaction_id);

        if (null == $transaction) {
            return Response::message('RESOURCE_NOT_FOUND');
        }

        $folder = storage_path() . '/payment-proof';
        $paymentProof = $request->payment_proof;

        if (!file_exists($folder)) {
            mkdir($folder);
        }

        $filename = sha1($request->transaction_id . '_' . date('YmdHis')) . '.' . $paymentProof->getClientOriginalExtension();
        $paymentProof->move($folder, $filename);

        $transaction->payment_id = $request->payment_id;
        $transaction->payment_proof = $filename;
        $transaction->status = 'lunas';
        $transaction->payment_date = Carbon::now()->toDateTimeString();
        $transaction->save();

        $transactionDetail = TransactionDetail::where('transaction_id', $request->transaction_id)->get();

        foreach ($transactionDetail as $detail) {
            $production = new Production();
            $production->transaction_detail_id = $detail->id;
            $production->notes = $request->notes;
            $production->status = 'desain';
            $production->date_in = Carbon::now()->toDateTimeString();
            $production->save();
        }

        return Response::send(200);
    }

    public function productions()
    {
        $transaction = Transaction::with('customer', 'TransactionDetail.production')->get();

        return Response::send(200, $transaction);
    }

    public function production(string $id)
    {
        $transaction = Transaction::with('customer', 'TransactionDetail.production')->where('id', $id)->first();

        return Response::send(200, $transaction);
    }
}

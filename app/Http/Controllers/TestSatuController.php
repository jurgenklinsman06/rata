<?php

namespace App\Http\Controllers;

use App\Responses\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestSatuController extends Controller
{
    public function index(Request $request)
    {
        $rules = [
            'diagonal_array' => 'required|array'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::send(422, $validator->errors());
        }

        $diagonalSatu = 0;
        $diagonalDua = 0;
        $difference = 0;
        $diagonalArray = $request->diagonal_array;
        $lastIndex = count($diagonalArray);

        for ($i = 0; $i < count($diagonalArray); $i++) {
            $diagonalSatu += $diagonalArray[$i][$i];
            $diagonalDua += $diagonalArray[$i][--$lastIndex];
        }

        $difference = $diagonalSatu - $diagonalDua;

        return Response::send(200, $difference);
    }
}

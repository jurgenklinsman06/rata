<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $table = 'transaction_detail';

    public function production()
    {
        return $this->hasOne(Production::class, 'transaction_detail_id', 'id');
    }
}
